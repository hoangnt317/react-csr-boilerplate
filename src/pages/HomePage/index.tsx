import dayjs from 'dayjs'
import { observer } from 'mobx-react'
import { useStores } from 'stores'

const HomePage = () => {
  const { spinnerStore } = useStores()

  console.log(dayjs().toDate(), dayjs().add(1, 'day').format('MM/DD/YYY'), dayjs().subtract(1, 'day').toDate())
  console.log(spinnerStore.isLoading)
  function toggleLoading() {
    spinnerStore.toggleLoading()
  }

  return <span onClick={toggleLoading}>HOME PAGE</span>
}

export default observer(HomePage)
